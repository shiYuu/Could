require.config({
  baseUrl: '/',
  paths: {
    jquery: 'static/js/jquery.min',
    Underscore: 'static/js/underscore-min',
    backbone: 'static/js/backbone.min',
  },
  shim: {                     //引入没有使用requirejs模块写法的类库。backbone依赖underscore
          'Underscore': {
              exports: '_'
          },
          'jquery': {
              exports: '$'
          },
          'zepto': {
              exports: '$'
          },
          'backbone': {
              deps: ['Underscore', 'jquery'],
              exports: 'Backbone'
          }
      }
})
